#include "mainwindow.h"
#include <QApplication>

#include "opencv2/opencv.hpp"
#include <iomanip>

using namespace cv;
using namespace std;

Point2f TestHomography(Mat h,Point2f c) {
    Point2f w;

    w.x = c.x * h.at<double>(0,0) + c.y * h.at<double>(0,1) + h.at<double>(0,2);
    w.y = c.x * h.at<double>(1,0) + c.y * h.at<double>(1,1) + h.at<double>(1,2);

    return w;
}

void onMouse(int evt, int x, int y, int flags, void* param) {
    if(evt == CV_EVENT_LBUTTONDOWN) {
        std::vector<cv::Point2f>* ptPtr = (std::vector<cv::Point2f>*)param;
        ptPtr->push_back(cv::Point2f(x,y));
        cout<<"("<<x<<','<<y<<")"<<endl;
    }
}

vector<Point2f> GetROI(Mat image){
    Mat img;
    image.copyTo(img);

    vector<Point2f> points;
    cv::namedWindow("Image Source");

    cv::setMouseCallback("Image Source", onMouse, (void*)&points);

    while(points.size()<4){
        cv::imshow("Image Source", img);

        waitKey(1);


        for (int i = 0; i<points.size(); ++i)
        {
            circle(img, Point(points[i].x,points[i].y),5, Scalar(0,0,0), -1);
            if (i<points.size()-1)
                line(img, Point(points[i].x,points[i].y), Point(points[i+1].x,points[i+1].y), Scalar(255,0,0), 3);
        }

    }

    line(img, Point(points[0].x,points[0].y), Point(points[3].x,points[3].y), Scalar(255,0,0), 3);
    cv::setMouseCallback("Image Source", NULL, NULL);
    cv::imshow("Image Source", img);
    waitKey(0);

    return points;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    // Read source image.
    Mat im_src = imread("/home/boukary/homograph_matrix/scene1/image00075.bmp");
    //Mat im_src = imread("/home/boukary/homograph_matrix/scene2/image00001.bmp");



    /*
    //scene1(60-90) ---
    vector<Point2f> pts_dst;
    pts_dst.push_back(Point2f(0, 0));
    pts_dst.push_back(Point2f(3, 0));
    pts_dst.push_back(Point2f(3, 26));
    pts_dst.push_back(Point2f(0, 26));

    //scene1(60-90) --- lane1
    vector<Point2f> pts_src1;
    pts_src1.push_back(Point2f(403, 104));
    pts_src1.push_back(Point2f(784, 108));
    pts_src1.push_back(Point2f(662, 734));
    pts_src1.push_back(Point2f(16, 720));

    //scene1(60-90) --- lane2
    vector<Point2f> pts_src2;
    pts_src2.push_back(Point2f(784, 108));
    pts_src2.push_back(Point2f(1168, 108));
    pts_src2.push_back(Point2f(1326, 740));
    pts_src2.push_back(Point2f(662, 734));
*/
    vector<Point2f> pts_src1;
    vector<Point2f> pts_src2;

    pts_src1 = GetROI(im_src);
    pts_src2 = GetROI(im_src);

    //scene2(90-120) ---
    vector<Point2f> pts_dst;
    pts_dst.push_back(Point2f(0, 0));
    pts_dst.push_back(Point2f(3, 0));
    pts_dst.push_back(Point2f(3, 13));
    pts_dst.push_back(Point2f(0, 13));

    /*
    //scene2(90-120) --- lane1
    vector<Point2f> pts_src1;
    pts_src1.push_back(Point2f(440, 136));
    pts_src1.push_back(Point2f(824, 142));
    pts_src1.push_back(Point2f(698, 858));
    pts_src1.push_back(Point2f(26, 822));

    //scene2(90-120) --- lane2
    vector<Point2f> pts_src2;
    pts_src2.push_back(Point2f(824, 142));
    pts_src2.push_back(Point2f(1204, 147));
    pts_src2.push_back(Point2f(1390, 869));
    pts_src2.push_back(Point2f(698, 858));*/



    //####################################################


    // Calculate Homography
    Mat h1 = findHomography(pts_src1, pts_dst,CV_RANSAC);
    Mat h2 = findHomography(pts_src2, pts_dst,CV_RANSAC);


    //cout<<"Homography Matrix Lane 1: "<<endl<<endl;
    for (int i = 0; i < h1.rows; ++i) {
        for (int j = 0; j < h1.cols; ++j) {
            cout<<fixed<<setprecision(8)<<h1.at<double>(i,j);
            if (j<h1.cols-1)
                cout<<"\t";
        }
        cout<<endl;
    }

    cout<<endl;

    //cout<<"Homography Matrix Lane 2: "<<endl<<endl;
    for (int i = 0; i < h2.rows; ++i) {
        for (int j = 0; j < h2.cols; ++j) {
            cout<<fixed<<setprecision(8)<<h2.at<double>(i,j);
            if (j<h2.cols-1)
                cout<<"\t";
        }
        cout<<endl;
    }

    int lineType = 8;

    /** Create some points */
    Point rook_points[1][4];
    rook_points[0][0] = pts_src1[0];
    rook_points[0][1] = pts_src1[1];
    rook_points[0][2] = pts_src1[2];
    rook_points[0][3] = pts_src1[3];

    const Point* ppt[1] = { rook_points[0] };
    int npt1[] = { 4 };

    fillPoly( im_src,
              ppt,
              npt1,
              1,
              Scalar( 255, 0, 0,100 ),
              lineType );

    rook_points[0][0] = pts_src2[0];
    rook_points[0][1] = pts_src2[1];
    rook_points[0][2] = pts_src2[2];
    rook_points[0][3] = pts_src2[3];

    ppt[1] = { rook_points[0] };
    int npt2[] = { 4 };

    fillPoly( im_src,
              ppt,
              npt2,
              1,
              Scalar( 0, 255, 0,100 ),
              lineType );


    // Display images
    imshow("Image Source", im_src);

    //Mat hh = Mat(3, 3, CV_64F, data );

    cout<<"Test Points Lane 1: "<<endl;
    cout<<pts_src1[0]<<"     "<<TestHomography(h1,pts_src1[0])<<endl;
    cout<<pts_src1[1]<<"     "<<TestHomography(h1,pts_src1[1])<<endl;
    cout<<pts_src1[2]<<"     "<<TestHomography(h1,pts_src1[2])<<endl;
    cout<<pts_src1[3]<<"     "<<TestHomography(h1,pts_src1[3])<<endl;

    cout<<"Test Points Lane 2: "<<endl;
    cout<<pts_src2[0]<<"     "<<TestHomography(h2,pts_src2[0])<<endl;
    cout<<pts_src2[1]<<"     "<<TestHomography(h2,pts_src2[1])<<endl;
    cout<<pts_src2[2]<<"     "<<TestHomography(h2,pts_src2[2])<<endl;
    cout<<pts_src2[3]<<"     "<<TestHomography(h2,pts_src2[3])<<endl;

    waitKey(0);

    return a.exec();
}
